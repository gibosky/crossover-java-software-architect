package com.crossover.techtrial.service;

import com.crossover.techtrial.dto.DailyElectricity;
import com.crossover.techtrial.model.HourlyElectricity;
import com.crossover.techtrial.repository.HourlyElectricityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DailyElectricityServiceImpl implements DailyElectricityService {

    @Autowired
    HourlyElectricityRepository hourlyElectricityRepository;

    @Override
    public List<DailyElectricity> getAllDailyElectricityByPanelId(Long panelId) {
        return generatingDailyValues(hourlyElectricityRepository.findAllByPanelId(panelId));
    }

    private List<DailyElectricity> generatingDailyValues(List<HourlyElectricity> hourlyElectricityList) {
        Map<String,DailyElectricity> dailyElectricityMap = new HashMap<>();
        Map<String,Double> dailyElectricityCountMap = new HashMap<>();

        for (int i = 0; i < hourlyElectricityList.size(); i++) {
            HourlyElectricity hourlyElectricity = hourlyElectricityList.get(i);
            String localDate = hourlyElectricity.getReadingAt().toLocalDate().toString();
            if (dailyElectricityMap.containsKey(localDate)) {
                DailyElectricity dailyElectricity = dailyElectricityMap.get(localDate);
                dailyElectricity.setSum(dailyElectricity.getSum() + hourlyElectricity.getGeneratedElectricity());
                if (hourlyElectricity.getGeneratedElectricity() > dailyElectricity.getMax()) {
                    dailyElectricity.setMax(hourlyElectricity.getGeneratedElectricity());
                } else if (hourlyElectricity.getGeneratedElectricity() < dailyElectricity.getMin()) {
                    dailyElectricity.setMin(hourlyElectricity.getGeneratedElectricity());
                }
                Double count = dailyElectricityCountMap.get(localDate) + 1;
                dailyElectricityCountMap.put(localDate, count);
                dailyElectricity.setAverage(dailyElectricity.getSum()/count);
            } else {
                dailyElectricityCountMap.put(localDate, 1d);
                DailyElectricity dailyElectricity = new DailyElectricity ();
                dailyElectricity.setDate(hourlyElectricity.getReadingAt().toLocalDate());
                dailyElectricity.setSum(hourlyElectricity.getGeneratedElectricity());
                dailyElectricity.setMax(hourlyElectricity.getGeneratedElectricity());
                dailyElectricity.setMin(hourlyElectricity.getGeneratedElectricity());
                dailyElectricity.setAverage(hourlyElectricity.getGeneratedElectricity().doubleValue());
                dailyElectricityMap.put(hourlyElectricity.getReadingAt().toLocalDate().toString(), dailyElectricity);
            }
        }

        return new ArrayList<>(dailyElectricityMap.values());
    }
}
